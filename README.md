# Simple RV32I Simulator

## Build and install

Building the simulator requires a current Rust installation. At the time of development rustc 1.61.0 stable was used and tested, but older version probably work too. Rust can be downloaded from [https://rustup.rs/](https://rustup.rs/).

The following instructions build and install the simulator:

```sh
cargo install cargo-deb
cargo deb
# <x.y.z> should be replaced by the current project version
# <targetarch> depends on the host maschine
sudo apt install ./target/debian/rv32i-sim_<x.y.z>_<targetarch>.deb
```

For development the simulator can be run via the `cargo` toolchain:

```sh
cargo run -- <options to pass to rv32i-sim>
```

---

## Usage

This simulator is intended to simulate firmware binaries, that target the [simple RV32I processor](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i). The simulator has the same fixed memory layout as the testbench memory of the processor and as implemented in the [spartan 7 demo project](https://r-n-d.informatik.hs-augsburg.de:8080/sose2022-hwsys-risc-v/simple-rv32i-spartan-7):

| Address         | Memory             |
|-----------------|--------------------|
| 0x0 - 0x999     | Combined ROM / RAM |
| 0x1000 - 0x1003 | general purpose ouput (i.e. leds at the example fpga project)|

A [suitable linker script](./simple-rv32i.ld) is contained in this repository.

For detailed usage of the simulator see:

```sh
rv32i-sim --help
```

The most important options are:

* Either `--bin <FILE>` or `--elf <FILE>` provides the path to the binary, which should be executed.
* `-d` starts the simulator in interactive debug mode. Type `h` or `help` for further information after starting the simulator.
* The simulator executes the binary, until the program counter stays at the same value for 2 cycles. This can be achieved i.e. with a C loop `while(1);` or in assembler with `catchloop:     j catchloop`.  Otherwise the simulator can execute a given number of cycles with the `-c <INT>` option.
* Generate memory access trace files for simple-rv32i testbenches with `--tracefile <FILE>`. The testbenches will call the simulator automatically
* Silent `--silent` mode only executes the simulator, but does not print the register content after each cycle.

The simulator uses the crate `env_logger` for logging. Set the environment variable `RUST_LOG` to the desired log level. I.e. `export RUST_LOG=info` on Linux systems.