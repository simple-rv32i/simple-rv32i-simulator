use clap::{arg, value_parser, ArgAction, ArgGroup, Command};
use log::{debug, error, info, warn};
use parse_int::parse;
use rvsim::{elf::Elf32, CpuState, Interp, Memory, MemoryAccess, Op, SimpleClock};
use std::fs::File;
use std::io::{Read, Write};
use std::path::PathBuf;
use std::string::String;

// elf constants
pub const ELF_SECTION_TYPE_NULL: u32 = 0;
pub const ELF_SECTION_TYPE_PROGBITS: u32 = 1;
pub const ELF_SECTION_TYPE_SYMTAB: u32 = 2;
pub const ELF_SECTION_TYPE_STRTAB: u32 = 3;
pub const ELF_SECTION_TYPE_RELA: u32 = 4;
pub const ELF_SECTION_TYPE_HASH: u32 = 5;
pub const ELF_SECTION_TYPE_DYNAMIC: u32 = 6;
pub const ELF_SECTION_TYPE_NOTE: u32 = 7;
pub const ELF_SECTION_TYPE_REL: u32 = 9;
pub const ELF_SECTION_TYPE_SHLIB: u32 = 10;
pub const ELF_SECTION_TYPE_DYNSYM: u32 = 11;
pub const ELF_SECTION_TYPE_NUM: u32 = 12;
pub const ELF_SECTION_TYPE_LOPROC: u32 = 0x70000000;
pub const ELF_SECTION_TYPE_HIPROC: u32 = 0x7fffffff;
pub const ELF_SECTION_TYPE_LOUSER: u32 = 0x80000000;
pub const ELF_SECTION_TYPE_HIUSER: u32 = 0xffffffff;

enum CliMemoryType {
    Bin(PathBuf),
    Elf(PathBuf),
}

#[derive(PartialEq)]
enum DebugAction{
    Run,
    Exit,
    Step,
    ContinueToPCAddr(u32),
}

struct Bram36Memory {
    dram: [u8; 4096],
    gpo_led: [u8; 4],
}

struct TraceItem {
    addr: u32,
    iswrite: bool,
    data_0: String,
    data_1: String,
    data_2: String,
    data_3: String,
}

impl Bram36Memory {
    fn from_bin(filename: PathBuf) -> Result<Self, std::io::Error> {
        let mut tempmem = Self {
            dram: [0xFF; 4096],
            gpo_led: [0; 4],
        };
        let mut readbuf = Vec::new();
        debug!("open bin file");
        let mut infile = File::open(filename)?;
        debug!("reading bin file");
        infile.read_to_end(&mut readbuf)?;
        let mut idx = 0;
        while idx < readbuf.len() && idx < tempmem.dram.len() {
            tempmem.dram[idx] = readbuf[idx];
            idx += 1;
        }
        debug!("reading bin file successfull");
        Ok(tempmem)
    }

    fn from_elf(filename: PathBuf) -> Result<Self, std::io::Error> {
        debug!("reading elf file {:?}", filename);
        let mut tempmem = Self {
            dram: [0xFF; 4096],
            gpo_led: [0; 4],
        };
        let mut elfbuf: Vec<u8> = Vec::new();
        let mut infile = File::open(filename)?;
        infile.read_to_end(&mut elfbuf)?;
        Self::debug_print_elf_content(Elf32::parse(elfbuf.as_ref()).unwrap());
        let elfcontent = Elf32::parse(elfbuf.as_ref()).unwrap();
        for (proghead_idx, proghead) in elfcontent.ph.into_iter().enumerate() {
            let phy_addr: u32 = proghead.paddr;
            if proghead.typ == ELF_SECTION_TYPE_PROGBITS {
                info!(
                    "laoding progbits into memory 0x{:08x} to 0x{:08x}",
                    phy_addr,
                    proghead.paddr + proghead.memsz - 1
                );
                if proghead.filesz == 0 {
                    info!("\tsection apparently RAM");
                }
            }
            let mut outofmemerr_flag = false;
            for idx in 0..proghead.memsz as usize {
                if proghead.paddr as usize + idx < tempmem.dram.len() {
                    if idx < proghead.filesz as usize {
                        tempmem.dram[phy_addr as usize + idx] = elfcontent.p[proghead_idx][idx];
                    } else {
                        // ram
                        tempmem.dram[phy_addr as usize + idx] = 0x00;
                    }
                } else {
                    outofmemerr_flag = true;
                }
            }
            if outofmemerr_flag {
                warn!(
                    "section starting at {} not (completely) in memory",
                    phy_addr
                );
            };
        }
        debug!("elf file successfully read to memory");
        Ok(tempmem)
    }

    fn try_get_mem_entry(&self, addr: u32) -> Option<u8> {
        if addr < self.dram.len() as u32 {
            Option::Some(self.dram[addr as usize])
        } else if addr < self.dram.len() as u32 + 4 {
            Option::Some(self.gpo_led[addr as usize - self.dram.len()])
        } else {
            Option::None
        }
    }
    fn debug_print_elf_content(elfcontent: Elf32) {
        debug!("{:?}", elfcontent);
        debug!("header: {:#?}", elfcontent.header);
        debug!("ident:  {:#?}", elfcontent.ident);
        debug!("=== {:#?} program headers ===", elfcontent.ph.len());
        for pheader in elfcontent.ph {
            debug!("{:?}", pheader);
        }
        debug!("=== {:#?} section headers ===", elfcontent.sh.len());
        for idx in 0..elfcontent.sh.len() {
            debug!("header: {:?}", elfcontent.sh[idx]);
            let header = elfcontent.sh[idx];
            if header.typ == ELF_SECTION_TYPE_STRTAB {
                let mut data = String::new();
                for datum in elfcontent.s[idx] {
                    if *datum != 0 {
                        data.push(*datum as char);
                    } else {
                        data.push('|');
                    }
                }
                debug!("chars: {}", data);
            } else if header.typ == ELF_SECTION_TYPE_NULL {
                debug!("null section, name: {}", header.name as u32)
            } else if header.typ == ELF_SECTION_TYPE_PROGBITS {
                debug!("progbits, name: {}", header.name as u32);
            } else if header.typ == ELF_SECTION_TYPE_SYMTAB {
                debug!("symbol table, name: {}", header.name as u32)
            } else {
                debug!(
                    "unkown section: {}, name: {}",
                    header.typ as u32, header.name as u32
                );
            }
        }
    }
}

impl Memory for Bram36Memory {
    fn access<T: Copy>(&mut self, addr: u32, access: MemoryAccess<T>) -> bool {
        if addr < 4096 {
            Memory::access(&mut self.dram[..], addr, access)
        } else if addr < 4097 {
            Memory::access(&mut self.gpo_led[..], addr - 4096, access)
        } else {
            false
        }
    }
}

impl TraceItem {
    fn headerline() -> &'static str {
        "address     iswrite     data\n"
    }
    fn dataline(&self) -> String {
        let boolstr = match self.iswrite {
            true => "True ",
            false => "False",
        };
        format!(
            "{:08x}    {}       {}{}{}{}\n",
            self.addr, boolstr, self.data_0, self.data_1, self.data_2, self.data_3
        )
    }
}

fn dump_reg(reg: &CpuState) {
    for idx in 0..16 {
        println!(
            "x{:02}: {:08x}\t\tx{:02}: {:08x}",
            idx * 2,
            reg.x[idx as usize * 2],
            idx * 2 + 1,
            reg.x[idx as usize * 2 + 1]
        );
    }
    println!("\npc: {:08x}", reg.pc);
}

// the simulation library supports rv32gc, a superset o RV32i, so check
// of the current instruction actually is a RV32I instruction
fn is_rv32i_instr(opc: Op) -> bool {
    debug!("check if instruction is contained in rv32i");
    match opc {
        Op::Lui { rd: _, u_imm: _ }
        | Op::Auipc { rd: _, u_imm: _ }
        | Op::Jal { rd: _, j_imm: _ }
        | Op::Jalr {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Beq {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bne {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Blt {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bge {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bltu {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bgeu {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Lb {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lh {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lw {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lbu {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lhu {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Sb {
            rs1: _,
            rs2: _,
            s_imm: _,
        }
        | Op::Sh {
            rs1: _,
            rs2: _,
            s_imm: _,
        }
        | Op::Sw {
            rs1: _,
            rs2: _,
            s_imm: _,
        }
        | Op::Addi {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Slti {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Sltiu {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Xori {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Ori {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Andi {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Slli {
            rd: _,
            rs1: _,
            shamt: _,
        }
        | Op::Srli {
            rd: _,
            rs1: _,
            shamt: _,
        }
        | Op::Srai {
            rd: _,
            rs1: _,
            shamt: _,
        }
        | Op::Add {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sll {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Slt {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sltu {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Xor {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Srl {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Or {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::And {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sub {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sra {
            rd: _,
            rs1: _,
            rs2: _,
        } => true,
        Op::Fence { pred: _, succ: _ } | Op::Ecall | Op::Ebreak => {
            error!("{:?} not implemented, but part of RV32I", opc);
            false
        }
        _ => {
            error!("{:?} not part of RV32I", opc);
            false
        }
    }
}

fn append_traceitems(
    tracevec: &mut Vec<TraceItem>,
    pc: u32,
    opc: Op,
    mem: &Bram36Memory,
    oldregs: [u32; 32],
) {
    debug!("storing trace data to memory");
    match opc {
        Op::Lui { rd: _, u_imm: _ }
        | Op::Auipc { rd: _, u_imm: _ }
        | Op::Jal { rd: _, j_imm: _ }
        | Op::Jalr {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Beq {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bne {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Blt {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bge {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bltu {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Bgeu {
            rs1: _,
            rs2: _,
            b_imm: _,
        }
        | Op::Lb {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lh {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lw {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lbu {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Lhu {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Sb {
            rs1: _,
            rs2: _,
            s_imm: _,
        }
        | Op::Sh {
            rs1: _,
            rs2: _,
            s_imm: _,
        }
        | Op::Sw {
            rs1: _,
            rs2: _,
            s_imm: _,
        }
        | Op::Addi {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Slti {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Sltiu {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Xori {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Ori {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Andi {
            rd: _,
            rs1: _,
            i_imm: _,
        }
        | Op::Slli {
            rd: _,
            rs1: _,
            shamt: _,
        }
        | Op::Srli {
            rd: _,
            rs1: _,
            shamt: _,
        }
        | Op::Srai {
            rd: _,
            rs1: _,
            shamt: _,
        }
        | Op::Add {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sll {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Slt {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sltu {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Xor {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Srl {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Or {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::And {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sub {
            rd: _,
            rs1: _,
            rs2: _,
        }
        | Op::Sra {
            rd: _,
            rs1: _,
            rs2: _,
        } => tracevec.push(TraceItem {
            addr: pc,
            iswrite: false,
            data_0: format!("{:02x}", mem.try_get_mem_entry(pc).unwrap()),
            data_1: format!("{:02x}", mem.try_get_mem_entry(pc + 1).unwrap()),
            data_2: format!("{:02x}", mem.try_get_mem_entry(pc + 2).unwrap()),
            data_3: format!("{:02x}", mem.try_get_mem_entry(pc + 3).unwrap()),
        }),
        _ => {
            warn!("unsupported opcode, trace file my is corrupted")
        }
    };
    // additional memory access with lx/sx
    match opc {
        Op::Lb { rd: _, rs1, i_imm } | Op::Lbu { rd: _, rs1, i_imm } => {
            let memidx = (oldregs[rs1] as i64 + i_imm as i64) as u32;
            tracevec.push(TraceItem {
                addr: memidx,
                iswrite: false,
                data_0: format!("{:02x}", mem.try_get_mem_entry(memidx).unwrap()),
                data_1: "XX".to_string(),
                data_2: "XX".to_string(),
                data_3: "XX".to_string(),
            });
        }
        Op::Lh { rd: _, rs1, i_imm } | Op::Lhu { rd: _, rs1, i_imm } => {
            let memidx = (oldregs[rs1] as i64 + i_imm as i64) as u32;
            tracevec.push(TraceItem {
                addr: memidx,
                iswrite: false,
                data_0: format!("{:02x}", mem.try_get_mem_entry(memidx).unwrap()),
                data_1: format!("{:02x}", mem.try_get_mem_entry(memidx + 1).unwrap()),
                data_2: "XX".to_string(),
                data_3: "XX".to_string(),
            });
        }
        Op::Lw { rd: _, rs1, i_imm } => {
            let memidx = (oldregs[rs1] as i64 + i_imm as i64) as u32;
            tracevec.push(TraceItem {
                addr: memidx,
                iswrite: false,
                data_0: format!("{:02x}", mem.try_get_mem_entry(memidx).unwrap()),
                data_1: format!("{:02x}", mem.try_get_mem_entry(memidx + 1).unwrap()),
                data_2: format!("{:02x}", mem.try_get_mem_entry(memidx + 2).unwrap()),
                data_3: format!("{:02x}", mem.try_get_mem_entry(memidx + 3).unwrap()),
            });
        }
        Op::Sb { rs1, rs2: _, s_imm } => {
            let memidx = (oldregs[rs1] as i64 + s_imm as i64) as u32;
            tracevec.push(TraceItem {
                addr: memidx,
                iswrite: true,
                data_0: format!("{:02x}", mem.try_get_mem_entry(memidx).unwrap()),
                data_1: "XX".to_string(),
                data_2: "XX".to_string(),
                data_3: "XX".to_string(),
            });
        }
        Op::Sh { rs1, rs2: _, s_imm } => {
            let memidx = (oldregs[rs1] as i64 + s_imm as i64) as u32;
            tracevec.push(TraceItem {
                addr: memidx,
                iswrite: true,
                data_0: format!("{:02x}", mem.try_get_mem_entry(memidx).unwrap()),
                data_1: format!("{:02x}", mem.try_get_mem_entry(memidx + 1).unwrap()),
                data_2: "XX".to_string(),
                data_3: "XX".to_string(),
            });
        }
        Op::Sw { rs1, rs2: _, s_imm } => {
            let memidx = (oldregs[rs1] as i64 + s_imm as i64) as u32;
            tracevec.push(TraceItem {
                addr: memidx,
                iswrite: true,
                data_0: format!("{:02x}", mem.try_get_mem_entry(memidx).unwrap()),
                data_1: format!("{:02x}", mem.try_get_mem_entry(memidx + 1).unwrap()),
                data_2: format!("{:02x}", mem.try_get_mem_entry(memidx + 2).unwrap()),
                data_3: format!("{:02x}", mem.try_get_mem_entry(memidx + 3).unwrap()),
            });
        }
        _ => {}
    }
}

/// returns 1 if continue or 0 if end of debug
fn debug_interaction(mem: &Bram36Memory) -> DebugAction {
    loop {
        let mut buffer = String::new();
        print!("> ");
        std::io::stdout().flush().expect("could not flush stdout");
        let stdin = std::io::stdin();
        stdin.read_line(&mut buffer).unwrap();
        buffer = buffer.to_lowercase();
        let line = buffer.as_str();
        if buffer.len() < 2 {
            continue;
        };
        let command = line.split_whitespace().next().unwrap();
        match command {
            "c" | "continue" => {
                return DebugAction::Run;
            },
            "cpc" | "continueuntilpc" => {
                if line.split_whitespace().count() < 2 {
                    println!("continue to pc adr, usage: cpc <addr>, but address not given. Try again");
                } else {
                    let rawaddr = line.split_whitespace().nth(1).unwrap();
                    if let Ok(addr) = parse::<u32>(rawaddr) {
                        return DebugAction::ContinueToPCAddr(addr);
                    }
                }
            },
            "e" | "exit" | "q" | "quit" => {
                return DebugAction::Exit;
            },
            "p" | "print" => {
                if line.split_whitespace().count() < 2 {
                    println!("print memory, usage: p <addr>, but address not given. Try again");
                } else {
                    let rawaddr = line.split_whitespace().nth(1).unwrap();
                    if let Ok(addr) = parse::<u32>(rawaddr) {
                        let mut data: [u8; 4] = [0; 4];
                        let mut address_error = false;
                        for idx in 0..4 {
                            if let Some(datum) = mem.try_get_mem_entry(addr + idx) {
                                data[idx as usize] = datum;
                            } else {
                                println!("address 0x{:08x} out of range", addr + idx);
                                address_error = true;
                                break;
                            }
                        }
                        if address_error {
                            continue;
                        }
                        println!(
                            "{:08x}: {:02x}{:02x}{:02x}{:02x}",
                            addr, data[0], data[1], data[2], data[3]
                        );
                    } else {
                        println!("ill formated address: '{}'", rawaddr);
                    }
                }
            },
            "s" | "step" => {
                break;
            },
            _ => {
                println!("commands:");
                println!("c|continue: continue (run until empty loop");
                println!("cpc|continueuntilpc <addr> continue until pc eqals pc");
                println!("q|e: exit");
                println!("s: step");
                println!("p <addr>: print memory address");
                println!("h:   help");
            }
        };
    }
    DebugAction::Step
}

fn main() {
    env_logger::init();
    // cli definition
    let matches = Command::new("rv32i-sim")
        .version(clap::crate_version!())
        .arg(
            arg!(
                -b --bin <FILE> "select a .bin file as memory input"
            )
            .value_parser(value_parser!(PathBuf)),
        )
        .arg(
            arg!(
                -e --elf <FILE> "select a .elf file as memory input"
            )
            .value_parser(value_parser!(PathBuf)),
        )
        .arg(
            arg!(
                -d --debug "enable debug mode for step to step execution"
            )
            .action(ArgAction::SetTrue),
        )
        .arg(
            arg!(
                --silent "do not print register content after each instruction"
            )
            .action(ArgAction::SetFalse),
        )
        .arg(
            arg!(
                -c --cycles <COUNT> "run COUNT cycles and stop after that"
            )
            .required(false)
            .default_value("-1")
            .value_parser(value_parser!(i64)),
        )
        .arg(
            arg!(
                -t --tracefile <FILE> "write memory trace data for testbenches"
            )
            .required(false)
            .value_parser(value_parser!(PathBuf)),
        )
        .group(ArgGroup::new("file").required(true).args(&["bin", "elf"]))
        .get_matches();
    let mut debug_action = DebugAction::Run;
    let debug = matches.get_one::<bool>("debug").unwrap();
    if *debug {
        debug!("run in debug mode");
        debug_action = DebugAction::Step;
    };
    let printregcontent = matches.get_one::<bool>("silent").unwrap();
    if *printregcontent {
        debug!("silent mode disabled");
    } else {
        debug!("silent mode enabled");
    };
    let cycles = matches.get_one::<i64>("cycles").unwrap();
    if *cycles <= 0 {
        debug!("run until loop is detected");
    } else {
        debug!("execute {} instructions and stop", *cycles);
    };
    let memtype = if let Some(mem) = matches.get_one::<PathBuf>("bin") {
        CliMemoryType::Bin((*mem).clone())
    } else {
        let pathbuf = (*matches.get_one::<PathBuf>("elf").unwrap()).clone();
        CliMemoryType::Elf(pathbuf)
    };
    let mut is_trace = false;
    let tracefilepath = match matches.get_one::<PathBuf>("tracefile") {
        Some(path) => {
            is_trace = true;
            path.clone()
        }
        None => PathBuf::default(),
    };
    if is_trace {
        debug!("write to trace file {:?}", tracefilepath);
    } else {
        debug!("do not write to trace file");
    };
    // setup cpu
    let mut mem = match memtype {
        CliMemoryType::Bin(filepath) => Bram36Memory::from_bin(filepath).unwrap(),
        CliMemoryType::Elf(filepath) => Bram36Memory::from_elf(filepath).unwrap(),
    };
    let mut clock = SimpleClock::new();
    // set pc to 0
    let mut cpustate = CpuState::new(0x0000_0000);
    let mut cpu = Interp::new(&mut cpustate, &mut mem, &mut clock);
    let mut cycle = 0i64;
    if *debug {
        println!("start debugging");
    }
    let mut last_pc = 0u32;
    let mut last_regfile_content: [u32; 32];
    let mut traceitems: Vec<TraceItem> = Vec::new();
    while *cycles < 0 || cycle < *cycles {
        match debug_action {
            DebugAction::Step => {
                debug_action = debug_interaction(cpu.mem);
            },
            DebugAction::Exit => {
                return;
            },
            DebugAction::ContinueToPCAddr(pcgoal) => {
                if pcgoal == cpu.state.pc {
                    debug_action = debug_interaction(cpu.mem);
                }
            },
            _ => {}
        };
        if *printregcontent {
            println!("\n--- cycle {} ---", cycle);
        };
        last_regfile_content = cpu.state.x;
        let stepres = cpu.step();
        if *printregcontent {
            dump_reg(cpu.state);
        }
        match stepres {
            Ok(opc) => {
                if !is_rv32i_instr(opc) {
                    panic!("not a implemented (~RV32I) instruction");
                } else {
                    println!("instr: {:?}", opc);
                }
            }
            Err(res) => panic!("error {:?} in instruction {:?}", res.1, res.0),
        };
        if is_trace {
            append_traceitems(
                &mut traceitems,
                last_pc,
                stepres.unwrap(),
                cpu.mem,
                last_regfile_content,
            )
        }
        if debug_action == DebugAction::Run && cpu.state.pc == last_pc {
            info!("cycle forever at {:08x} detected, exiting", last_pc);
            break;
        } else {
            last_pc = cpu.state.pc;
        }
        cycle += 1;
    }
    if is_trace {
        debug!("simulation finished, writing trace data to file");
        let mut tracefile = File::create(tracefilepath).unwrap();
        tracefile
            .write_all(TraceItem::headerline().as_bytes())
            .unwrap();
        for item in traceitems {
            tracefile.write_all(item.dataline().as_bytes()).unwrap();
        }
    };
    debug!("exiting successfull");
}
